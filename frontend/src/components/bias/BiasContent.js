import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";

const BiasContent = ({ biasContent, fetchAllBiases }) => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [example, setExample] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [modalSaveOpen, setModalSaveOpen] = useState(false);

    useEffect(() => {
        setName(biasContent.name);
        setDescription(biasContent.description);
        setExample(biasContent.example);
    }, [biasContent]);

    const putUpdatedBias = async (e) => {
        e.preventDefault();
        const updatedBias = {
            id: biasContent.id,
            name: name,
            description: description,
            example: example
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedBias);
        }
    }

    const putRequest = async (updatedBias) => {
        await fetch(`http://localhost:8080/api/bias/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedBias)
        });
        fetchAllBiases(); // Refreshes the list and loads the buttons
    }

    const deleteBias = async () => {
        await fetch(`http://localhost:8080/api/bias/delete/${biasContent.id}`, {
            method: 'DELETE'
        });
        fetchAllBiases();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <form className="biasContent-form" onSubmit={putUpdatedBias}>
                <label>Biasnaam</label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <label>Biasomschrijving</label>
                <textarea value={description} onChange={e => setDescription(e.target.value)} />
                <label>Voorbeeld</label>
                <textarea value={example} onChange={e => setExample(e.target.value)} />
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder bias</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteBias, putUpdatedBias)}
        </div>
    );
}

export default BiasContent;