package com.biasbrothers.seriousgame.round;

import com.biasbrothers.seriousgame.biasQuestion.BiasQuestion;
import com.biasbrothers.seriousgame.canvas.Canvas;
import com.biasbrothers.seriousgame.scenario.Scenario;
import com.biasbrothers.seriousgame.timer.Timer;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Round")
@Table(
        name = "round"
)
public class Round {

    @Id
    @SequenceGenerator(
            name = "round_sequence",
            sequenceName = "round_sequence",
            initialValue = 7,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "round_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;

    @Column(
            name = "title",
            nullable = false
    )
    private String title;

    @Column(
            name = "round_number",
            nullable = false
    )
    private String roundNumber;

    @ManyToMany(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST})
    @JoinTable(name = "canvas_round",
            joinColumns = @JoinColumn(name = "canvas_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "round_id",
                    referencedColumnName = "id"))
    private List<Canvas> canvases;

    @ManyToMany(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST})
    @JoinTable(name = "bias_question_round",
            joinColumns = @JoinColumn(name = "bias_question_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "round_id",
                    referencedColumnName = "id"))
    private List<BiasQuestion> biasQuestions;

    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST})
    @JoinColumn(name = "scenario_id")
    private Scenario scenario;

    @ManyToOne(cascade ={CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST})
    @JoinColumn(name = "timer_id")
    private Timer timer;

    public Round() {

    }

    public Round(String title, String roundNumber, List<Canvas> canvases, List<BiasQuestion> biasQuestions, Scenario scenario, Timer timer) {
        this.title = title;
        this.roundNumber = roundNumber;
        this.canvases = canvases;
        this.biasQuestions = biasQuestions;
        this.scenario = scenario;
        this.timer = timer;
    }

    public Round(Long id, String title, String roundNumber, List<Canvas> canvases, List<BiasQuestion> biasQuestions, Scenario scenario, Timer timer) {
        this.id = id;
        this.title = title;
        this.roundNumber = roundNumber;
        this.canvases = canvases;
        this.biasQuestions = biasQuestions;
        this.scenario = scenario;
        this.timer = timer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Canvas> getCanvases() {
        return canvases;
    }

    public void setCanvases(List<Canvas> canvases) {
        this.canvases = canvases;
    }

    public List<BiasQuestion> getBiasQuestions() {
        return biasQuestions;
    }

    public void setBiasQuestions(List<BiasQuestion> biasQuestions) {
        this.biasQuestions = biasQuestions;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(String round) {
        this.roundNumber = round;
    }

    public void add(Canvas canvas, BiasQuestion biasQuestion) {
        canvases.add(canvas);
        biasQuestions.add(biasQuestion);
    }

    public void remove(Canvas canvas, BiasQuestion biasQuestion) {
        canvases.remove(canvas);
        biasQuestions.remove(biasQuestion);
    }

    @Override
    public String toString() {
        return "Round{" +
                "id=" + id +
                ", canvases=" + canvases +
                ", biasQuestions=" + biasQuestions +
                ", scenario=" + scenario +
                ", timer=" + timer +
                '}';
    }
}
