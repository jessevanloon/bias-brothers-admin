package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.timer.Timer;
import com.biasbrothers.seriousgame.timer.TimerController;
import com.biasbrothers.seriousgame.timer.TimerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TimerController.class)
public class TimerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TimerService timerService; // mock the repository

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findTimerByIdTest() throws Exception {
        Timer timer = new Timer(
                1L,
                "5"
        );

        Mockito.doReturn(timer).when(timerService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(timer);

        this.mvc.perform(get("/timer/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(timerService).getById(1L);
    }

    @Test
    public void addTimerTest() throws Exception {
        Timer timer = new Timer(
                1L,
                "5"
        );

        Mockito.when(timerService.add(Mockito.any(Timer.class))).thenReturn(timer);

        final String expectedResponseContent = objectMapper.writeValueAsString(timer);

        this.mvc.perform(post("/timer/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteTimerTest() throws Exception {
        Mockito.when(timerService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/timer/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateTimerTest() throws Exception {
        Timer timer = new Timer(
                1L,
                "test"
        );

        Mockito.when(timerService.update(Mockito.any(Timer.class))).thenReturn(timer);

        final String expectedResponseContent = objectMapper.writeValueAsString(timer);

        this.mvc.perform(put("/timer/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }
}
