package com.biasbrothers.seriousgame.utils.jsonexporter.rounddownloader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("download_round")
public class DownloadRoundController {

    private final RoundExporterService roundExporterService;

    @Autowired
    public DownloadRoundController(RoundExporterService roundExporterService) {
        this.roundExporterService = roundExporterService;
    }

    @GetMapping("/{roundNumber}")
    public ResponseEntity<byte[]> downloadRound(@PathVariable Long roundNumber) {
        String roundJsonString = roundExporterService.roundJson(roundNumber);

        byte[] roundJsonBytes = roundJsonString.getBytes();

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=round" + roundNumber + ".json")
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(roundJsonBytes.length)
                .body(roundJsonBytes);

    }
}

