import Modal from "../components/modal/Modal";

const checkModal = (modalIsOpen, modalSaveOpen, closeModalHandler, deleteFunc, putFunc) => {
    if (modalIsOpen) {
        return <Modal cancelHandler={closeModalHandler} confirmHandler={deleteFunc} modalQuestion='Weet je zeker dat je de content wilt verwijderen?' modalCancelText='Annuleren' modalConfirmText='Verwijderen' />;
    }
    if (modalSaveOpen) {
        return <Modal cancelHandler={closeModalHandler} confirmHandler={putFunc} modalQuestion='Weet je zeker dat je de wijzigingen wilt opslaan?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />;
    }
    else return <></>
}

export default checkModal;