import { useState, useEffect, useCallback } from "react";
import ButtonList from "../ButtonList";
import CanvasButtons from "./GameCanvasButtons";
import GameCanvasNewsArticles from "../game/GameCanvasNewsArticles";
import GameCanvasMetrics from "../game/GameCanvasMetrics";

const GameMenu = () => {
    const [roundList, setRoundList] = useState([]); // Sets the list with all rounds and canvasses
    const [chosenRound, setChosenRound] = useState(0); // Integer to check which round has been chosen. Is used to load correct canvasses
    const [canvasContent, setCanvasContent] = useState(false); // When a canvas is clicked, the content gets set to be used in other components, like GameCanvasNewsArticles
    const [roundSelected, setRoundSelected] = useState(false); // Keeps track if a round is selected. If not, all round buttons show. If yes, the correct Canvas buttons show
    const [shownContent, setShownContent] = useState(''); // Keeps track of which content to show (newsarticles, biases, scenarios etc). 
    const [showContentButtons, setShowContentButtons] = useState(false); // Keeps track if to show the content buttons or not, if a canvas is selected
    const listForButtons = [
        {
            id: 1,
            name: "Ronde 1"
        }, {
            id: 2,
            name: "Ronde 2"
        }, {
            id: 3,
            name: "Ronde 3"
        }, {
            id: 4,
            name: "Ronde 4"
        }, {
            id: 5,
            name: "Ronde 5"
        }, {
            id: 6,
            name: "Ronde 6"
        }
    ];

    const fetchAllCanvas = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/canvas');
        const data = await res.json();
        sortCanvasList(data);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllCanvas();
        }
        fetchData();
    }, [fetchAllCanvas]);

    const sortCanvasList = (data) => {
        const sortedOnId = data.sort((a, b) => a.id - b.id);
        setRoundList([
            {
                canvas1: null,
                canvas2: null,
                canvas3: sortedOnId[0],
                canvas4: null,
                canvas5: null
            },
            {
                canvas1: null,
                canvas2: sortedOnId[1],
                canvas3: sortedOnId[2],
                canvas4: sortedOnId[3],
                canvas5: null
            },
            {
                canvas1: sortedOnId[4],
                canvas2: sortedOnId[5],
                canvas3: sortedOnId[6],
                canvas4: sortedOnId[7],
                canvas5: sortedOnId[8]
            },
            {
                canvas1: sortedOnId[9],
                canvas2: sortedOnId[10],
                canvas3: sortedOnId[11],
                canvas4: sortedOnId[12],
                canvas5: sortedOnId[13]
            },
            {
                canvas1: sortedOnId[14],
                canvas2: sortedOnId[15],
                canvas3: sortedOnId[16],
                canvas4: sortedOnId[17],
                canvas5: sortedOnId[18]
            },
            {
                canvas1: sortedOnId[19],
                canvas2: sortedOnId[20],
                canvas3: sortedOnId[21],
                canvas4: sortedOnId[22],
                canvas5: sortedOnId[23]
            }
        ]);
    }

    const handleRoundClick = (clickedRoundId) => {
        setCanvasContent(false);
        setChosenRound(clickedRoundId);
        setRoundSelected(true);
    }

    const goBack = () => {
        setRoundSelected(false);
        setChosenRound(0);
        setCanvasContent(false);
        setShownContent('');
        setShowContentButtons(false);
    }

    const putRequest = async (updatedCanvas) => {
        await fetch(`http://localhost:8080/api/canvas/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedCanvas)
        });
        await fetchAllCanvas();
        setShownContent('');
        setShowContentButtons(false);
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {chosenRound > 0 ? <h3>Ronde: {chosenRound}</h3> : <h3>Kies ronde</h3>}
                {roundSelected && <button onClick={goBack}>Terug</button>}
                {!roundSelected && <ButtonList list={listForButtons} handleItemClick={handleRoundClick} color={chosenRound} />}
                {roundSelected && <CanvasButtons list={roundList[chosenRound-1]} setCanvasContent={setCanvasContent} setShownContent={setShownContent} setShowContentButtons={setShowContentButtons} showContentButtons={showContentButtons} />}
                {showContentButtons && (
                    <div>
                        <button onClick={() => setShownContent("scenario")} id={shownContent === "scenario" ? "button-clicked" : undefined} style={{ marginTop: "60px" }}>Scenario's</button>
                        <button onClick={() => setShownContent("biases")} id={shownContent === "biases" ? "button-clicked" : undefined}>Biases</button>
                        <button onClick={() => setShownContent("newsarticles")} id={shownContent === "newsarticles" ? "button-clicked" : undefined}>Nieuwsberichten</button>
                        <button onClick={() => setShownContent("map")} id={shownContent === "map" ? "button-clicked" : undefined}>Kaart</button>
                        <button onClick={() => setShownContent("metrics")} id={shownContent === "metrics" ? "button-clicked" : undefined}>Metrieken</button>
                    </div>
                )}
            </div>
            <div className="container-content">
                {shownContent === "newsarticles" && <GameCanvasNewsArticles content={canvasContent} putRequest={putRequest} /> }
                {shownContent === "metrics" && <GameCanvasMetrics content={canvasContent} putRequest={putRequest} />}
            </div>
        </div>
    );
}

export default GameMenu;
