import { useState } from "react";
import Modal from "../modal/Modal";
import Backdrop from "../modal/Backdrop";

const MapAdd = ({ fetchAllMaps }) => {
    const [name, setName] = useState('');
    const [url, setUrl] = useState('');
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postMap = async (e) => {
        e.preventDefault();
        if (name === '' || url === '') {
            setEmptyFieldModal(true);
            return;
        }
        const map = {
            name: name,
            url: url
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(map);
        }
    }

    const postRequest = async (map) => {
        await fetch('http://localhost:8080/api/map/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(map)
        })
        fetchAllMaps();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <>
            <form className="mapContent-form" onSubmit={postMap}>
                <label id="mapNaam">Nieuwe naam: </label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <label>Nieuwe image url: </label>
                <textarea value={url} onChange={e => setUrl(e.target.value)} />
                <label>Preview </label>
                <iframe className="text-area-maps" src={url} width="640" height="480" allow="autoplay" title={"preview"} />
                <div className="div-save-button">
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
                <div>
                    {/* Opens the modal for the save button. */}
                    {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postMap} modalQuestion='Weet je zeker dat je een nieuwe kaart wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                    {alertModal && <Backdrop cancelModal={closeModalHandler} />}

                    {/* Opens the modal when there are empty fields */}
                    {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                    {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
                </div>
            </form>
        </>
    );
}

export default MapAdd;
