package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestion;
import com.biasbrothers.seriousgame.scenario.Scenario;
import com.biasbrothers.seriousgame.scenario.ScenarioController;
import com.biasbrothers.seriousgame.scenario.ScenarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ScenarioController.class)
public class ScenarioControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ScenarioService scenarioService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findScenarioByIdTest() throws Exception {
        List<MeasureQuestion> measureQuestions = new ArrayList<>();
        measureQuestions.add(new MeasureQuestion(5 , "We plaatsen een overzicht van de informatie over het virus en omschrijven welke soorten middelen infectie kunnen afremmen of tegenhouden."));
        measureQuestions.add(new MeasureQuestion(0 , "De bevolking van Digitanzanië is slim genoeg om te snappen dat dit soort huis, tuin en keukenmiddeltjes hen niet veilig gaan houden. We hoeven niks te doen."));
        measureQuestions.add(new MeasureQuestion(2 , "Een gevoel van veiligheid is ook wat waard, maar dat moet niet ten koste gaan van anderen. We laten weten dat de tampons niet zullen helpen."));
        Scenario scenario = new Scenario(
                "Hamsterwoede!",
                "Met oplopende Olifantengriepcijfers op de horizon zijn inwoners van Engelse Eiland massaal begonnen met het inslaan van tampons. Deze zouden helpen de Olifantengriep uit de neus te houden en daarmee een infectie te voorkomen. De regering van Engelse Eiland verzoekt haar burgers dringend om te stoppen met hamsteren, en verzekert hen ervan dat de voorraden groot genoeg zijn om iedereen te kunnen bedienen. Over de effectiviteit van het tampongebruik als preventie van de Olifantengriep zijn door experts nog geen uitspraken gedaan. In Digitanzanië beginnen de eerste geluiden al op te gaan om spullen in te slaan. Wat moet de overheid doen?", measureQuestions);
                Mockito.doReturn(scenario).when(scenarioService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(scenario);

        this.mvc.perform(get("/scenario/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(scenarioService).getById(1L);
    }

    @Test
    public void addNewScenarioTest() throws Exception {
        MeasureQuestion question1 = new MeasureQuestion(0 , "");
        MeasureQuestion question2 = new MeasureQuestion(0 , "");
        MeasureQuestion question3 = new MeasureQuestion(0 , "");
        List<MeasureQuestion> measureQuestions = new ArrayList<>();
        measureQuestions.add(question1);
        measureQuestions.add(question2);
        measureQuestions.add(question3);
        Scenario scenario = new Scenario("add", "add", measureQuestions);

        Mockito.when(scenarioService.add(Mockito.any(Scenario.class))).thenReturn(scenario);

        final String expectedResponseContent = objectMapper.writeValueAsString(scenario);

        this.mvc.perform(post("/scenario/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteScenarioTest() throws Exception {
        Mockito.when(scenarioService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/scenario/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateScenarioTest() throws Exception {
        MeasureQuestion question1 = new MeasureQuestion(0 , "");
        MeasureQuestion question2 = new MeasureQuestion(0 , "");
        MeasureQuestion question3 = new MeasureQuestion(0 , "");
        List<MeasureQuestion> measureQuestions = new ArrayList<>();
        measureQuestions.add(question1);
        measureQuestions.add(question2);
        measureQuestions.add(question3);
        Scenario scenario = new Scenario("update", "update", measureQuestions);
        Mockito.doReturn(scenario).when(scenarioService).add(scenario);

        Mockito.when(scenarioService.update(Mockito.any(Scenario.class))).thenReturn(scenario);

        final String expectedResponseContent = objectMapper.writeValueAsString(scenario);

        this.mvc.perform(put("/scenario/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }

}
