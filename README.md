**About this project**

Before this project we (a group of students) automated and digitalized a Serious Game for a Dutch company called Bias Brothers. As a second project, their wish was to be able to update/modify the content of this game.

In this application you can read, update, add, or delete all types of game content. The next step is to make the updated JSON files compatible with the game.

**To run the project**
Navigate to the frontend folder and type this command:

npm install


This will install all necessary packages.
The backend can be started with IntelliJ. When the backend is running, run npm start and the application should be up and running!

**Getting Started with Create React App**
This project was bootstrapped with Create React App.

Available Scripts
In the project directory, you can run:

**npm start**

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

**npm test**

Launches the test runner in the interactive watch mode.
See the section about running tests for more information.

**npm run build**

Builds the app for production to the build folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.
Your app is ready to be deployed!
See the section about deployment for more information.
