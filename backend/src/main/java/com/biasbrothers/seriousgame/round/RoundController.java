package com.biasbrothers.seriousgame.round;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/round")
public class RoundController {
    private final RoundService roundService;

    @Autowired
    public RoundController(RoundService roundService) {
        this.roundService = roundService;
    }

    @CrossOrigin
    @GetMapping
    public List<Round> getAll() {
        return roundService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Round getById(@RequestParam Long id) {
        return roundService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Round> add(@RequestBody Round round) {
        Round roundTest = roundService.add(round);

        return new ResponseEntity<>(roundTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        roundService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update")
    public ResponseEntity<Round> update(@RequestBody Round round) {
        Round roundTest = roundService.update(round);

        return new ResponseEntity<>(roundTest, HttpStatus.ACCEPTED);
    }
}
