import { useState, useEffect } from "react"
import Backdrop from "../modal/Backdrop";
import ScenarioForm from "./ScenarioForm";
import checkModal from "../../Functions/checkModal";

const ScenarioContent = ({ scenarioContent, fetchAllScenarios }) => {
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');
    const [measureQuestion1, setMeasureQuestion1] = useState({
        id: '',
        answer: '',
        points: ''
    });
    const [measureQuestion2, setMeasureQuestion2] = useState({
        id: '',
        answer: '',
        points: ''
    });
    const [measureQuestion3, setMeasureQuestion3] = useState({
        id: '',
        answer: '',
        points: ''
    });

    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setTitle(scenarioContent.title);
        setText(scenarioContent.text);
        setMeasureQuestion1({ id: scenarioContent.measureQuestions[0].id, answer: scenarioContent.measureQuestions[0].answer, points: scenarioContent.measureQuestions[0].points });
        setMeasureQuestion2({ id: scenarioContent.measureQuestions[1].id, answer: scenarioContent.measureQuestions[1].answer, points: scenarioContent.measureQuestions[1].points });
        setMeasureQuestion3({ id: scenarioContent.measureQuestions[2].id, answer: scenarioContent.measureQuestions[2].answer, points: scenarioContent.measureQuestions[2].points });
    }, [scenarioContent]);

    const putUpdatedScenario = async (e) => {
        e.preventDefault();
        const updatedScenario = {
            id: scenarioContent.id,
            measureQuestions: [measureQuestion1, measureQuestion2, measureQuestion3],
            title: title,
            text: text
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedScenario);
        }
    }

    const putRequest = async (updatedScenario) => {
        await fetch(`http://localhost:8080/api/scenario/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedScenario)
        });
        fetchAllScenarios(); // Refreshes the list and loads the buttons
    }

    const deleteScenario = async () => {
        await fetch(`http://localhost:8080/api/scenario/delete/${scenarioContent.id}`, {
            method: 'DELETE'
        });
        fetchAllScenarios();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <ScenarioForm postOrPut={putUpdatedScenario}
                title={title} setTitle={setTitle}
                text={text} setText={setText}
                measureQuestion1={measureQuestion1} setMeasureQuestion1={setMeasureQuestion1}
                measureQuestion2={measureQuestion2} setMeasureQuestion2={setMeasureQuestion2}
                measureQuestion3={measureQuestion3} setMeasureQuestion3={setMeasureQuestion3}
                openModalHandler={openModalHandler}
                greyedOut={true}
            />
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteScenario, putUpdatedScenario)}
        </div>
    );
}

export default ScenarioContent;
