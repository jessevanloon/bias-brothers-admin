import { useState } from "react";

const CanvasButtons = ({ list, setCanvasContent, setShownContent, setShowContentButtons, showContentButtons }) => {

    const [color, setColor] = useState(false);

    const handleClick = (content, id) => {
        setCanvasContent(content);
        setColor(id);
        setShownContent(0);
        setShowContentButtons(true);
    }

    return (
        <>
            {list.canvas1 !== null && (<button onClick={() => handleClick(list.canvas1, 1)} id={(color === 1 && showContentButtons) ? "button-clicked" : undefined}>Canvas 1</button>)}
            {list.canvas2 !== null && (<button onClick={() => handleClick(list.canvas2, 2)} id={(color === 2 && showContentButtons) ? "button-clicked" : undefined}>Canvas 2</button>)}
            <button onClick={() => handleClick(list.canvas3, 3)} id={(color === 3 && showContentButtons) ? "button-clicked" : undefined}>Canvas 3</button>
            {list.canvas4 !== null && (<button onClick={() => handleClick(list.canvas4, 4)} id={(color === 4 && showContentButtons) ? "button-clicked" : undefined}>Canvas 4</button>)}
            {list.canvas5 !== null && (<button onClick={() => handleClick(list.canvas5, 5)} id={(color === 5 && showContentButtons) ? "button-clicked" : undefined}>Canvas 5</button>)}
        </>
    );
}

export default CanvasButtons;
