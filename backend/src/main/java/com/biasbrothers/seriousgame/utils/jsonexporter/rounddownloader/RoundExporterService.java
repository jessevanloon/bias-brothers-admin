package com.biasbrothers.seriousgame.utils.jsonexporter.rounddownloader;

import com.biasbrothers.seriousgame.round.Round;
import com.biasbrothers.seriousgame.round.RoundService;
import com.biasbrothers.seriousgame.utils.jsonexporter.JsonExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoundExporterService {

    private final RoundService roundService;
    private final JsonExporter jsonExporter;

    @Autowired
    public RoundExporterService(RoundService roundService, JsonExporter jsonExporter) {
        this.roundService = roundService;
        this.jsonExporter = jsonExporter;
    }

    String roundJson(Long roundNumber) {
        Round fetchedRound = roundService.getById(roundNumber);

        if (roundNumber >= 1 && roundNumber <= roundService.getAll().size()) {
            return jsonExporter.exportJson(fetchedRound);
        } else {
            throw new IllegalStateException("Round number "  + roundNumber + " does not exist");
        }
    }


}
