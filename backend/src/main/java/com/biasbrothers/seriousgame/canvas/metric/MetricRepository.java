package com.biasbrothers.seriousgame.canvas.metric;

import com.biasbrothers.seriousgame.biasQuestion.BiasQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface MetricRepository extends JpaRepository<Metric, Long> {

    @Query("SELECT p FROM Metric p WHERE LOWER(p.name) LIKE LOWER(concat('%', ?1,'%'))")
    public List<Metric> search(String keyword);
}
