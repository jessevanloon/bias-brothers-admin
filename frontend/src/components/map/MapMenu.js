import { useCallback, useEffect, useState } from "react"
import MapAdd from "./MapAdd";
import MapContent from "./MapContent";
import ButtonList from "../ButtonList";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";
import SearchIcon from '@mui/icons-material/Search';

const MapMenu = () => {
    const [mapList, setMapList] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [mapContent, setMapContent] = useState([]);
    const [newMap, addMap] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllMaps = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/map');
        const data = await res.json();
        setMapList(data);
        setButtons(data)
        setChosenIndex(false);
        addMap(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllMaps();
        }
        fetchData();
    }, [fetchAllMaps]);

    const handleMapClick = (clickedMapId) => {
        mapList.forEach(map => {
            if (map.id === clickedMapId) {
                addMap(false);
                setMapContent(map);
                setChosenIndex(clickedMapId);
                setOpenModalMenu(false);
                setSearchInput('');
            }
        });
    }

    const clickAddMap = () => {
        setChosenIndex(false);
        addMap(true)
    }

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach(map => {
            const button = { id: map.id, name: map.name }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setSearchInput('');
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    // Set the list for the modalmenu
    const returnFilteredData = (searchValue) => {
        if (searchValue !== '') {
            return mapList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            });
        } else {
            return mapList;
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {mapList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleMapClick} color={chosenIndex} />}
                <button onClick={clickAddMap} title="addMap">+</button>
            </div>
            <div className="container-content">
                {!newMap && !chosenIndex ? 'Selecteer een kaart.' : ''}
                {chosenIndex && <MapContent mapContent={mapContent} fetchAllMaps={fetchAllMaps} />}
                {newMap ? <MapAdd fetchAllMaps={fetchAllMaps} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddMap}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleMapClick} listForButtons={returnFilteredData(searchInput)} listForResults={(e) => setSearchInput(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    )
}

export default MapMenu;
