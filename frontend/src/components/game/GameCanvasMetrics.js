import { useState, useEffect, useCallback } from "react";

const GameCanvasMetrics = ({ content, putRequest }) => {
    const [listForDropdown, setListForDropdown] = useState([]);
    const [metric1, setMetric1] = useState(content.metrics[0].name);
    const [metric2, setMetric2] = useState(content.metrics[1].name);
    const [metric3, setMetric3] = useState(content.metrics[2].name);
    const [value1, setValue1] = useState(content.value1);
    const [value2, setValue2] = useState(content.value2);
    const [value3, setValue3] = useState(content.value3);
    const [idMetric1, setIdMetric1] = useState(content.metrics[0].id);
    const [idMetric2, setIdMetric2] = useState(content.metrics[1].id);
    const [idMetric3, setIdMetric3] = useState(content.metrics[2].id);

    const fetchMetrics = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/metric');
        const data = await res.json();
        setListForDropdown(data);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchMetrics();
        }
        fetchData();
    }, [fetchMetrics, content]);

    const handleDropdown = (e, index) => {
        listForDropdown.forEach(metric => {
            if (index === 1) {
                if (e.target.value === metric.name) {
                    setMetric1(e.target.value);
                    setIdMetric1(metric.id);
                }
            } else if (index === 2) {
                if (e.target.value === metric.name) {
                    setMetric2(e.target.value);
                    setIdMetric2(metric.id);
                }
            } else if (index === 3) {
                if (e.target.value === metric.name) {
                    setMetric3(e.target.value);
                    setIdMetric3(metric.id);
                }
            }
        });
    }

    const updateGameCanvasMetrics = (e) => {
        e.preventDefault();
        const updatedCanvas = content;
        updatedCanvas.metrics[0].name = metric1;
        updatedCanvas.metrics[1].name = metric2;
        updatedCanvas.metrics[2].name = metric3;
        updatedCanvas.value1 = value1;
        updatedCanvas.value2 = value2;
        updatedCanvas.value3 = value3;
        updatedCanvas.metrics[0].id = idMetric1;
        updatedCanvas.metrics[1].id = idMetric2;
        updatedCanvas.metrics[2].id = idMetric3;
        putRequest(updatedCanvas);
    }

    return (
        <div className="div-content">
            {listForDropdown.length > 0 && (
                <form className="canvasmetrics-form" onSubmit={updateGameCanvasMetrics}>
                    <div className="canvas-form-row-label">
                        <label>Metriek 1</label>
                        <select onChange={e => handleDropdown(e, 1)}>
                            <option defaultValue="Kies een andere metriek">Kies een andere metriek</option>
                            {listForDropdown.map(metric => <option key={metric.id} value={metric.name}>{metric.name}</option>)}
                        </select>
                    </div>
                    <div className="canvas-form-row-values">
                        <textarea className="greyed-out" value={metric1} readOnly/>
                        <textarea value={value1} onChange={e => setValue1(e.target.value)}/>
                    </div>
                    <div className="canvas-form-row-label">
                        <label>Metriek 2</label>
                        <select onChange={e => handleDropdown(e, 2)}>
                            <option defaultValue="Kies een andere metriek">Kies een andere metriek</option>
                            {listForDropdown.map(metric => <option key={metric.id} value={metric.name}>{metric.name}</option>)}
                        </select>
                    </div>
                    <div className="canvas-form-row-values">
                        <textarea className="greyed-out" value={metric2} readOnly/>
                        <textarea value={value2} onChange={e => setValue2(e.target.value)}/>
                    </div>
                    <div className="canvas-form-row-label">
                        <label>Metriek 3</label>
                        <select onChange={e => handleDropdown(e, 3)}>
                            <option defaultValue="Kies een andere metriek">Kies een andere metriek</option>
                            {listForDropdown.map(metric => <option key={metric.id} value={metric.name}>{metric.name}</option>)}
                        </select>
                    </div>
                    <div className="canvas-form-row-values">
                        <textarea className="greyed-out" value={metric3} readOnly/>
                        <textarea value={value3} onChange={e => setValue3(e.target.value)}/>
                    </div>
                    <div className="div-save-button">
                        <input className="button-save" type="submit" value="Opslaan"/>
                    </div>
                </form>
            )}
        </div>
    );
}

export default GameCanvasMetrics;
