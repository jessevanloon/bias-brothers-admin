import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";

const TimerContent = ({ timerContent, fetchAllTimers }) => {
    const [name, setName] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setName(timerContent.name);
    }, [timerContent]);

    const putUpdatedTimer = async (e) => {
        e.preventDefault();
        const updatedTimer = {
            id: timerContent.id,
            name: name
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedTimer);
        }
    }

    const putRequest = async (updatedTimer) => {
        await fetch(`http://localhost:8080/api/timer/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedTimer)
        });
        fetchAllTimers(); // Refreshes the list and loads the buttons
    }

    const deleteTimer = async () => {
        await fetch(`http://localhost:8080/api/timer/delete/${timerContent.id}`, {
            method: 'DELETE'
        });
        fetchAllTimers();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <form className="timerContent-form" onSubmit={putUpdatedTimer}>
                <label>Aantal minuten: </label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder timer</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteTimer, putUpdatedTimer)}
        </div>
    );
}

export default TimerContent;