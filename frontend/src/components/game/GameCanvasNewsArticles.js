import { useState, useEffect, useCallback } from "react";

const GameCanvasNewsArticles = ({ content, putRequest }) => {
    const [chosenIndex, setChosenIndex] = useState(false);
    const [newsArticles, setNewsArticles] = useState([]);
    const [listForDropdown, setListForDropdown] = useState([]);
    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    const [source, setSource] = useState('');
    const [popUp, setPopup] = useState(false);
    const [id, setId] = useState(false);

    const fetchNewsArticles = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/news_article');
        const data = await res.json();
        setListForDropdown(data);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchNewsArticles();
            setNewsArticles(content.newsArticles);
        }
        fetchData();
    }, [fetchNewsArticles, content]);

    const handleChosenIndex = (counter) => {
        setChosenIndex(counter);
        setTitle(newsArticles[counter].title);
        setMessage(newsArticles[counter].message);
        setSource(newsArticles[counter].source);
        setPopup(newsArticles[counter].popUp);
        setId(newsArticles[counter].id);
    }

    const handleDropdownSelect = (e) => {
        listForDropdown.forEach(article => {
            if (e.target.value === article.title) {
                setTitle(article.title);
                setMessage(article.message);
                setSource(article.source);
                setPopup(article.popUp);
                setId(article.id);
            }
        })
    }

    const updateNewsArticleInCanvas = (e) => {
        e.preventDefault();
        const updatedCanvas = content;
        const newNewsArticle = {
            id: id,
            title: title,
            message: message,
            source: source,
            popUp: popUp
        };
        switch (chosenIndex) {
            case 0:
                updatedCanvas.newsArticle1 = newNewsArticle;
                break;
            case 1:
                updatedCanvas.newsArticle2 = newNewsArticle;
                break;
            case 2:
                updatedCanvas.newsArticle3 = newNewsArticle;
                break;
            default:
            // Just to get rid of warning
        }
        putRequest(updatedCanvas);
    }

    return (
        <div className="div-content">
            <div className="btn-group-navbar" style={{ display: "flex", justifyContent: "center" }}>
                {newsArticles.length > 0 && newsArticles.map((article, counter) => (
                    <button key={counter} onClick={() => handleChosenIndex(counter, article)}
                        id={chosenIndex === counter ? "button-clicked" : undefined}>Nieuwsbericht {counter + 1}</button>
                ))}
            </div>
            {chosenIndex !== false && (
                <>
                    <select style={{ marginTop: "5px", marginBottom: "5px" }} onChange={e => handleDropdownSelect(e)}>
                        <option defaultValue="Kies een ander nieuwsartikel">Kies een ander nieuwsartikel</option>
                        {listForDropdown.map((newsArticle) => <option key={newsArticle.id} value={newsArticle.title}>{newsArticle.title}</option>)}
                    </select>
                    <form className="newsArticleContent-form" onSubmit={updateNewsArticleInCanvas}>
                        <label>Nieuwsartikel titel</label>
                        <textarea value={title} className="greyed-out" />
                        <label>Nieuwsartikel inhoud</label>
                        <textarea value={message} className="greyed-out" />
                        <label>Nieuwsartikel bron</label>
                        <textarea value={source} className="greyed-out" />
                        <label>Nieuwsartikel pop-up</label>
                        <input type="checkbox" value={popUp} onChange={e => setPopup(e.target.checked)} className="checkbox-popup" checked={popUp} />
                        <div className="div-save-button">
                            <input className="button-save" type="submit" value="Opslaan" />
                        </div>
                    </form>
                </>
            )}
        </div>
    );
}

export default GameCanvasNewsArticles;
