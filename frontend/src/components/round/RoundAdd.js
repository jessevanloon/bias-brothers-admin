import { useState, useEffect } from "react";
import RoundForm from "./RoundForm";
import Backdrop from "../modal/Backdrop";
import Modal from "../modal/Modal";

const RoundAdd = ({ fetchAllRounds }) => {
    const [round, setRound] = useState({
        title: "",
        roundNumber: "",
        canvases: [
            {
                id: 25,
                name: "Leeg canvas",
                points: 0,
                map: null,
                value1: null,
                value2: null,
                value3: null,
                metrics: [],
                newsArticles: []
            },
            {
                id: 25,
                name: "Leeg canvas",
                points: 0,
                map: null,
                value1: null,
                value2: null,
                value3: null,
                metrics: [],
                newsArticles: []
            },
            {
                id: 25,
                name: "Leeg canvas",
                points: 0,
                map: null,
                value1: null,
                value2: null,
                value3: null,
                metrics: [],
                newsArticles: []
            },
            {
                id: 25,
                name: "Leeg canvas",
                points: 0,
                map: null,
                value1: null,
                value2: null,
                value3: null,
                metrics: [],
                newsArticles: []
            },
            {
                id: 25,
                name: "Leeg canvas",
                points: 0,
                map: null,
                value1: null,
                value2: null,
                value3: null,
                metrics: [],
                newsArticles: []
            }
        ],
        biasQuestions: [
            {
                id: 1,
                points: 0,
                bias: {
                    id: 0,
                    name: "",
                    description: "",
                    example: ""
                }
            },
            {
                id: 2,
                points: 2,
                bias: {
                    id: 0,
                    name: "",
                    description: "",
                    example: ""
                }
            },
            {
                id: 3,
                points: 5,
                bias: {
                    id: 0,
                    name: "",
                    description: "",
                    example: ""
                }
            }
        ],
        scenario: {
            id: 0,
            title: "",
            text: "",
            measureQuestions: [
                {
                    id: 0,
                    points: 0,
                    answer: ""
                },
                {
                    id: 2,
                    points: 0,
                    answer: ""
                },
                {
                    id: 3,
                    points: 2,
                    answer: ""
                }
            ]
        },
        timer: {
            id: 0,
            name: ""
        }
    });
    const [biasDropdown, setBiasDropdown] = useState([]);
    const [scenarioDropdown, setScenarioDropdown] = useState([]);
    const [timerDropdown, setTimerDropdown] = useState([]);
    const [canvasDropdown, setCanvasDropdown] = useState([]);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    useEffect(() => {
        fetchAllBiases();
        fetchAllScenarios();
        fetchAllTimers();
        fetchAllCanvas();
    }, []);

    const postRound = async (e) => {
        e.preventDefault();
        if (round.canvases[0].name === 'Leeg canvas' && round.canvases[1].name === 'Leeg canvas' && round.canvases[2].name === 'Leeg canvas'
            && round.canvases[3].name === 'Leeg canvas' && round.canvases[4].name === 'Leeg canvas') {
            setEmptyFieldModal(true);
            return;
        }
        setAlertModal(true);
        if (alertModal) {
            await postRequest();
        }
    }

    const postRequest = async () => {
        await fetch('http://localhost:8080/api/round/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(round)
        })
        fetchAllRounds();
    }

    const fetchAllBiases = async () => {
        const res = await fetch('http://localhost:8080/api/bias');
        const data = await res.json();
        setBiasDropdown(data);
    }

    const fetchAllScenarios = async () => {
        const res = await fetch('http://localhost:8080/api/scenario');
        const data = await res.json();
        setScenarioDropdown(data);
    }

    const fetchAllTimers = async () => {
        const res = await fetch('http://localhost:8080/api/timer');
        const data = await res.json();
        setTimerDropdown(data);
    }

    const fetchAllCanvas = async () => {
        const res = await fetch('http://localhost:8080/api/canvas');
        const data = await res.json();
        setCanvasDropdown(data);
    }

    const handleBiasDropdown = (e, index) => {
        const updatedBiasQuestions = round.biasQuestions;
        biasDropdown.forEach(bias => {
            if (index === 1) {
                if (e.target.value === bias.name) {
                    updatedBiasQuestions[0].bias = bias;
                    setRound({ ...round, biasQuestions: updatedBiasQuestions });
                }
            } else if (index === 2) {
                if (e.target.value === bias.name) {
                    updatedBiasQuestions[1].bias = bias;
                    setRound({ ...round, biasQuestions: updatedBiasQuestions });
                }
            } else if (index === 3) {
                if (e.target.value === bias.name) {
                    updatedBiasQuestions[2].bias = bias;
                    setRound({ ...round, biasQuestions: updatedBiasQuestions });
                }
            }
        });
    }

    const handleCanvasDropdown = (e, index) => {
        const updatedCanvases = round.canvases;
        canvasDropdown.forEach(canvas => {
            if (index === 1) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[0] = canvas;
                    setRound({ ...round, canvases: updatedCanvases });
                }
            } else if (index === 2) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[1] = canvas;
                    setRound({ ...round, canvases: updatedCanvases });
                }
            } else if (index === 3) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[2] = canvas;
                    setRound({ ...round, canvases: updatedCanvases });
                }
            } else if (index === 4) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[3] = canvas;
                    setRound({ ...round, canvases: updatedCanvases });
                }
            } else if (index === 5) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[4] = canvas;
                    setRound({ ...round, canvases: updatedCanvases });
                }
            }
        });
    }

    const handleScenarioDropdown = (e) => {
        scenarioDropdown.forEach(scenario => {
            if (e.target.value === scenario.title) {
                setRound({ ...round, scenario: scenario });
            }
        });
    }

    const handleTimerDropdown = (e) => {
        timerDropdown.forEach(timer => {
            if (e.target.value === timer.name) {
                setRound({ ...round, timer: timer });
            }
        });
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <div className="div-content">
            <RoundForm postOrPut={postRound} round={round} setRound={setRound} biasDropdown={biasDropdown}
                scenarioDropdown={scenarioDropdown} timerDropdown={timerDropdown} canvasDropdown={canvasDropdown}
                handleBiasDropdown={handleBiasDropdown} handleCanvasDropdown={handleCanvasDropdown} handleScenarioDropdown={handleScenarioDropdown}
                handleTimerDropdown={handleTimerDropdown} openModalHandler={openModalHandler} />
                            {/* Opens the modal for the save button. */}
            {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postRound} modalQuestion='Weet je zeker dat je een nieuwe ronde wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
            {alertModal && <Backdrop cancelModal={closeModalHandler} />}
            {/* Opens the modal when there are empty fields */}
            {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
            {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}

        </div>
    );
};

export default RoundAdd;
