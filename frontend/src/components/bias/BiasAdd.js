import { useState } from "react";
import Backdrop from "../modal/Backdrop";
import Modal from "../modal/Modal";


const BiasAdd = ({ fetchAllBiases }) => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [example, setExample] = useState('');
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postBias = async (e) => {
        e.preventDefault();
        if (name === '' || description === '' || example === '') {
            setEmptyFieldModal(true);
            return;
        }
        const bias = {
            name: name,
            description: description,
            example: example
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(bias);
        }
    }

    const postRequest = async (bias) => {
        await fetch('http://localhost:8080/api/bias/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bias)
        })
        fetchAllBiases();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <>
            <form className="biasContent-form" onSubmit={postBias}>
                <label id="biasNaam">Nieuwe biasnaam</label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <label>Nieuwe biasomschrijving</label>
                <textarea value={description} onChange={e => setDescription(e.target.value)} />
                <label>Nieuw biasvoorbeeld</label>
                <textarea value={example} onChange={e => setExample(e.target.value)} />
                <div className="div-save-button">
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
                <div>
                    {/* Opens the modal for the save button. */}
                    {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postBias} modalQuestion='Weet je zeker dat je een nieuwe Bias wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                    {alertModal && <Backdrop cancelModal={closeModalHandler} />}

                    {/* Opens the modal when there are empty fields */}
                    {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                    {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
                </div>
            </form>
        </>
    );
}

export default BiasAdd;
