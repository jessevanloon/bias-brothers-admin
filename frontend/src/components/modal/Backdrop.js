const Backdrop = ({ cancelModal }) => {
    return (
        <div className="backdrop" onClick={cancelModal} />
    );
}

export default Backdrop;
