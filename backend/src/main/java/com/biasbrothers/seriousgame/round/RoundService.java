package com.biasbrothers.seriousgame.round;

import com.biasbrothers.seriousgame.bias.BiasRepository;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestion;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestionRepository;
import com.biasbrothers.seriousgame.canvas.Canvas;
import com.biasbrothers.seriousgame.canvas.CanvasRepository;
import com.biasbrothers.seriousgame.canvas.map.MapRepository;
import com.biasbrothers.seriousgame.canvas.metric.Metric;
import com.biasbrothers.seriousgame.canvas.metric.MetricRepository;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestion;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestionRepository;
import com.biasbrothers.seriousgame.newsarticle.NewsArticle;
import com.biasbrothers.seriousgame.newsarticle.NewsArticleRepository;
import com.biasbrothers.seriousgame.scenario.ScenarioRepository;
import com.biasbrothers.seriousgame.timer.TimerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoundService {
    private final RoundRepository roundRepository;
    private final BiasQuestionRepository biasQuestionRepository;
    private final BiasRepository biasRepository;
    private final CanvasRepository canvasRepository;
    private final MapRepository mapRepository;
    private final MetricRepository metricRepository;
    private final NewsArticleRepository newsArticleRepository;
    private final ScenarioRepository scenarioRepository;
    private final MeasureQuestionRepository measureQuestionRepository;
    private final TimerRepository timerRepository;

    @Autowired
    public RoundService(RoundRepository roundRepository, BiasQuestionRepository biasQuestionRepository, BiasRepository biasRepository, CanvasRepository canvasRepository, MapRepository mapRepository, MetricRepository metricRepository, NewsArticleRepository newsArticleRepository, ScenarioRepository scenarioRepository, MeasureQuestionRepository measureQuestionRepository, TimerRepository timerRepository) {
        this.roundRepository = roundRepository;
        this.biasQuestionRepository = biasQuestionRepository;
        this.biasRepository = biasRepository;
        this.canvasRepository = canvasRepository;
        this.mapRepository = mapRepository;
        this.metricRepository = metricRepository;
        this.newsArticleRepository = newsArticleRepository;
        this.scenarioRepository = scenarioRepository;
        this.measureQuestionRepository = measureQuestionRepository;
        this.timerRepository = timerRepository;
    }

    public List<Round> getAll() {
        return roundRepository.findAll();
    }

    public Round getById(Long id) {
        return roundRepository.findById(id).orElseThrow(() -> new IllegalStateException(
                "round with id " + id + " does not exist"));
    }

    public Round add(Round round) {
        Round newRound = new Round();

        return saveRound(round, newRound);
    }

    public String delete(Long id) {
        roundRepository.deleteById(id);
        return "SUCCESS";
    }

    public Round update(Round round) {
        Round newRound = roundRepository.findById(
                round.getId()).orElseThrow(() -> new IllegalStateException(
                "round with id " + round.getId() + " does not exist"));

        return saveRound(round, newRound);
    }

    private Round saveRound(Round round, Round newRound) {
        newRound.setTitle(round.getTitle());
        newRound.setRoundNumber(round.getRoundNumber());

        for (BiasQuestion biasQuestion : round.getBiasQuestions()) {
            biasRepository.save(biasQuestion.getBias().clone());
        }

        for (Canvas canvas : round.getCanvases()) {
            for (NewsArticle newsArticle : canvas.getNewsArticles()) {
                newsArticleRepository.save(newsArticle.clone());
            }
        }

        for (Canvas canvas : round.getCanvases()) {
            for (Metric metric : canvas.getMetrics()) {
                metricRepository.save(metric.clone());
            }
        }

        for (Canvas canvas : round.getCanvases()) {
            mapRepository.save(canvas.getMap().clone());
        }

        for (MeasureQuestion measureQuestion : round.getScenario().getMeasureQuestions()) {
            measureQuestionRepository.save(measureQuestion.clone());
        }

        newRound.setTimer(timerRepository.save(round.getTimer()));
        newRound.setBiasQuestions(biasQuestionRepository.saveAll(round.getBiasQuestions()));
        newRound.setCanvases(canvasRepository.saveAll(round.getCanvases()));
        newRound.setScenario(scenarioRepository.save(round.getScenario().clone()));

        return roundRepository.save(newRound);
    }
}
