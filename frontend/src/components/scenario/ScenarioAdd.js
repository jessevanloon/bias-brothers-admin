import { useState } from "react";
import ScenarioForm from "./ScenarioForm";
import Modal from "../modal/Modal";
import Backdrop from "../modal/Backdrop";

function ScenarioAdd({ fetchAllScenarios }) {
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');
    const [measureQuestion1, setMeasureQuestion1] = useState({
        id: '',
        answer: '',
        points: ''
    });
    const [measureQuestion2, setMeasureQuestion2] = useState({
        id: '',
        answer: '',
        points: ''
    });
    const [measureQuestion3, setMeasureQuestion3] = useState({
        id: '',
        answer: '',
        points: ''
    });
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postScenario = async (e) => {
        e.preventDefault();
        if (title === '' || text === '') {
            setEmptyFieldModal(true);
            return;
        }
        const scenario = {
            title: title,
            text: text,
            measureQuestions: [measureQuestion1, measureQuestion2, measureQuestion3]
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(scenario);
        }
    }

    const postRequest = async (scenario) => {
        await fetch('http://localhost:8080/api/scenario/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(scenario)
        })
        fetchAllScenarios();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <>
            <ScenarioForm postOrPut={postScenario}
                title={title} setTitle={setTitle}
                text={text} setText={setText}
                measureQuestion1={measureQuestion1} setMeasureQuestion1={setMeasureQuestion1}
                measureQuestion2={measureQuestion2} setMeasureQuestion2={setMeasureQuestion2}
                measureQuestion3={measureQuestion3} setMeasureQuestion3={setMeasureQuestion3}
                greyedOut={false}
            />
            <div>
                {/* Opens the modal for the save button. */}
                {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postScenario} modalQuestion='Weet je zeker dat je een nieuw scenario wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                {alertModal && <Backdrop cancelModal={closeModalHandler} />}

                {/* Opens the modal when there are empty fields */}
                {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
            </div>
        </>
    );
}

export default ScenarioAdd;